function layThongTinTuForm() {
  var tkNv = document.getElementById("tknv").value.trim();
  var tenNv = document.getElementById("name").value.trim();
  var emailNv = document.getElementById("email").value.trim();
  var mkNV = document.getElementById("password").value.trim();
  var ngayLamNV = document.getElementById("datepicker").value.trim();
  var luongCBNV = document.getElementById("luongCB").value * 1;
  var chucVuNV = document.getElementById("chucvu").value;
  var gioLamNV = document.getElementById("gioLam").value * 1;

  var nvNew = new nhanVien(
    tkNv,
    tenNv,
    emailNv,
    mkNV,
    ngayLamNV,
    luongCBNV,
    chucVuNV,
    gioLamNV
  );
  return nvNew;
}

function renderDanhSachNV(list) {
  var contentHTML = "";
  for (var i = 0; i < list.length; i++) {
    var currentNV = list[i];
    var content = `<tr>
                <td>${currentNV.tk}</td>
                <td>${currentNV.ten}</td>
                <td>${currentNV.email}</td>
                <td>${currentNV.ngayLam}</td>
                <td>${currentNV.chucVu}</td>
                <td>${currentNV.tongLuong()}</td>
                <td>${currentNV.xepHang()}</td>
                <td>

                  <button 
                  onclick="suaThongTinNv(${currentNV.tk})"
                  class="btn btn-success" 
                  data-toggle="modal"
                  data-target="#myModal">Cập Nhập Lại</button>

                  <button onclick = "xoaNhanVien(${currentNV.tk})" 
                  class="btn btn-danger">Xóa</button>
                </td>
                </tr>`;
    contentHTML = contentHTML + content;
  }
  document.getElementById("tableDanhSach").innerHTML = contentHTML;
}

function showThongTinLenForm(nhanVien) {
  document.getElementById("tknv").value = nhanVien.tk;
  document.getElementById("name").value = nhanVien.ten;
  document.getElementById("email").value = nhanVien.email;
  document.getElementById("password").value = nhanVien.matKhau;
  document.getElementById("datepicker").value = nhanVien.ngayLam;
  document.getElementById("chucvu").value = nhanVien.chucVu;
  document.getElementById("luongCB").value = nhanVien.luongCB;
  document.getElementById("gioLam").value = nhanVien.gioLam;
}

function resetForm() {
  document.getElementById("formQLNV").reset();
}
