function kiemTraRong(value, idError) {
  if (value.length == 0) {
    document.getElementById(idError).innerHTML = "Không được để rỗng";
    return false;
  } else {
    document.getElementById(idError).innerHTML = "";
    return true;
  }
}

function kiemTraTk(tkNv, dssnv, idError) {
  var index = dssnv.findIndex(function (nv) {
    return nv.tk == tkNv;
  });

  if (index == -1) {
    document.getElementById(idError).innerHTML = "";
    return true;
  } else {
    document.getElementById(idError).innerHTML =
      "Tài khoãn đã tồn tại hoặc không hơp lệ";
    return false;
  }
}

function kiemTraThichHopLength(value, min, max, idError) {
  if (value.length >= min && value.length <= max) {
    document.getElementById(idError).innerHTML = "";
    return true;
  } else {
    document.getElementById(idError).innerHTML = "Phải đủ chiều dài từ 4 đến";
    return false;
  }
}

function kiemTraThichHopSoluong(value, min, max, idError) {
  if (value >= min && value <= max) {
    document.getElementById(idError).innerHTML = "";
    return true;
  } else {
    document.getElementById(idError).innerHTML =
      "Định lượng quá lơn hoặc quá nhỏ";
    return false;
  }
}

function kiemTraChuMatKhau(value, idError) {
  const re = "^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{6,}$";
  var isPass = re.match(value);
  if (!isPass) {
    document.getElementById(idError).innerText =
      "Mật khẩu cầu 1 ký tự đặc biêt, 1 hoa, 1 chữ số";
    return false;
  } else {
    document.getElementById(idError).innerText = "";
    return true;
  }
}

function kiemTraEmail(value, idError) {
  const re =
    /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;

  var isEmail = re.test(value);
  if (!isEmail) {
    document.getElementById(idError).innerText = "Email không hợp lệ";
    return false;
  } else {
    document.getElementById(idError).innerText = "";
    return true;
  }
}
