const DSSNV = "DSSNV";

var dssnv = [];

var dataJson = localStorage.getItem(DSSNV);
if (dataJson) {
  dataRaw = JSON.parse(dataJson);

  dssnv = dataRaw.map(function (item) {
    var nvNew = new nhanVien(
      item.tk,
      item.ten,
      item.email,
      item.matKhau,
      item.ngayLam,
      item.chucVu,
      item.luongCB,
      item.gioLam
    );

    return nvNew;
  });

  renderDanhSachNV(dssnv);
}
function saveLocalStorage() {
  // lưu xuống local
  var dssnvJson = JSON.stringify(dssnv);
  localStorage.setItem(DSSNV, dssnvJson);
}

function checkValid(nhanVien) {
  var isValid = true;

  // Kiểm tra tài khoản và ngày làm không để trống
  isValid =
    isValid & kiemTraRong(nhanVien.tk, "tbTKNV") &&
    kiemTraTk(nhanVien.tk, dssnv, "tbTKNV") &&
    kiemTraThichHopLength(nhanVien.tk, 4, 6, "tbTKNV") &&
    kiemTraRong(nhanVien.ngayLam, "tbNgay");

  // Kiểm tra tên
  isValid = isValid & kiemTraRong(nhanVien.ten, "tbTen");

  // Kiểm tra mật khẩu

  isValid =
    isValid & kiemTraRong(nhanVien.matKhau, "tbMatKhau") &&
    kiemTraThichHopLength(nhanVien.matKhau, 6, 10, "tbMatKhau") &&
    kiemTraChuMatKhau(nhanVien.matKhau, "tbMatKhau");

  //  Kiểm tra  email

  isValid = isValid & kiemTraEmail(nhanVien.email, "tbEmail");

  // Kiểm tra lương và kiểm  tra số giờ, kiểm tra chức vụ
  isValid =
    isValid &
      kiemTraThichHopSoluong(
        nhanVien.luongCB,
        1000000,
        20000000,
        "tbLuongCB"
      ) &&
    kiemTraThichHopSoluong(nhanVien.gioLam, 80, 200, "tbGiolam") &&
    kiemTraRong(nhanVien.chucVu, "tbChucVu");

  return isValid;
}
function themNv() {
  var newNV = layThongTinTuForm();
  var isValid = checkValid(newNV);

  if (isValid) {
    dssnv.push(newNV);
    saveLocalStorage();
    renderDanhSachNV(dssnv);
    resetForm();
  }
}

function xoaNhanVien(tkNv) {
  var index = dssnv.findIndex(function (nv) {
    return nv.tk == tkNv;
  });

  if (index == -1) {
    return;
  }
  // xóa sinh viên
  dssnv.splice(index, 1);
  renderDanhSachNV(dssnv);
}

function suaThongTinNv(tkNv) {
  var index = dssnv.findIndex(function (nv) {
    return nv.tk == tkNv;
  });

  showThongTinLenForm(dssnv[index]);

  document.getElementById("tkNv").disable = true;
}

function capNhapNv() {
  var nvEdit = layThongTinTuForm();
  var isValid = checkValid(newNV);
  if (isValid) {
    var index = dssnv.findIndex(function (nv) {
      return nv.tk == nvEdit.ma;
    });

    dssnv[index] = nvEdit;
    saveLocalStorage();
    renderDssv(dssnv);
    resetForm();
    dssnv[index] = document.getElementById("tknv").disable = false;
  }
}
function timNhanVien() {
  var hang = document.getElementById("searchName").value.trim();
  console.log("hang: ", hang);
  var newList = [];

  for (var i = 0; i < dssnv.length; i++) {
    if (dssnv[i].xepHang() == hang) {
      newList.push(dssnv[i]);
    }
  }
  renderDanhSachNV(newList);
}
