function nhanVien(
  _tk,
  _name,
  _email,
  _matKhau,
  _ngayLam,
  _LuongCB,
  _chucVu,
  _gioLam
) {
  this.tk = _tk;
  this.ten = _name;
  this.email = _email;
  this.matKhau = _matKhau;
  this.ngayLam = _ngayLam;
  this.chucVu = _chucVu;
  this.luongCB = _LuongCB;
  this.gioLam = _gioLam;
  // pương thức tính tổng lương
  this.tongLuong = function () {
    var tl = _LuongCB;
    switch (this.chucVU) {
      case "Sếp":
        tl *= 3;
      case "Trưởng phòng":
        tl *= 2;
      case "Nhân viên":
        tl *= 1;
    }
    return tl;
  };
  // Phương thức tính xếp hạng
  this.xepHang = function () {
    var hang = "trung bình";
    if (this.gioLam >= 192) {
      hang = "xuất sắc";
      return hang;
    } else if (this.gioLam < 192 && this.gioLam >= 176) {
      hang = "giỏi";
      return hang;
    } else if (this.gioLam < 176 && this.gioLam >= 160) {
      hang = "khá";
      return hang;
    } else {
      return hang;
    }
  };
}
